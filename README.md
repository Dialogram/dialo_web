# Dialogram Web Page

Dialogram is a collaborative platform, available on mobile and web, which aims to provide interpretations in French Sign Language (LSF) of media and documents.

## Dialogram Web

This repository contain the source code of the Dialogram Web page. This web only showcase Dialogram, our main repositories are [dialo_back](https://gitlab.com/Dialogram/dialo_back) for the back-end and [dialo_app](https://gitlab.com/Dialogram/dialo_web) for the front-end.

## Documentation

Please read the [documentation](https://dialogram.github.io/dialo_back_doc/) of the project.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Dialogram Team** - [Members](https://gitlab.com/groups/Dialogram/-/group_members)

See also the list of [contributors](https://gitlab.com/Dialogram/dialo_back/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
